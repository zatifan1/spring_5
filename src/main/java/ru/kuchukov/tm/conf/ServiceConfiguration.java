package ru.kuchukov.tm.conf;

import org.apache.cxf.Bus;
import org.apache.cxf.bus.spring.SpringBus;
import org.apache.cxf.jaxws.EndpointImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.kuchukov.tm.endpointservice.AuthServiceEP;
import ru.kuchukov.tm.endpointservice.ProjectServiceEP;
import ru.kuchukov.tm.endpointservice.TaskServiceEP;

import javax.xml.ws.Endpoint;

@Configuration
@ComponentScan(basePackages = "ru.kuchukov.tm")
public class ServiceConfiguration {

    @Autowired
    private ProjectServiceEP projectServiceEP;

    @Autowired
    private TaskServiceEP taskServiceEP;

    @Autowired
    private AuthServiceEP authServiceEP;

    @Bean(name = Bus.DEFAULT_BUS_ID)
    public SpringBus springBus() {
        return new SpringBus();
    }

    @Bean
    public Endpoint projectEndpoint() {
        EndpointImpl endpoint = new EndpointImpl(springBus(), projectServiceEP);
        endpoint.publish("/projectservice");
        return endpoint;
    }

    @Bean
    public Endpoint taskEndpoint() {
        EndpointImpl endpoint = new EndpointImpl(springBus(), taskServiceEP);
        endpoint.publish("/taskservice");
        return endpoint;
    }

    @Bean
    public Endpoint authEndpoint() {
        EndpointImpl endpoint = new EndpointImpl(springBus(), authServiceEP);
        endpoint.publish("/login");
        return endpoint;
    }

}
