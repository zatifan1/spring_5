package ru.kuchukov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.NoArgsConstructor;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kuchukov.tm.api.service.IUserService;
import ru.kuchukov.tm.entity.Domain;
import ru.kuchukov.tm.entity.User;
import ru.kuchukov.tm.repository.IUserRepository;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor

public class UserService extends AbstractService<User> implements IUserService<User> {

    @Autowired
    private IUserRepository userRepository;

    @Override
    @Transactional
    public User save(@NotNull final User user) {
        return userRepository.save(user);
    }

    @NotNull
    @Override
    public User findOneByEntity(@NotNull final User user) throws Exception {
        final Optional<User> optionalUser = userRepository.findById(user.getId());
        @NotNull final User newUser = optionalUser.orElseThrow(() -> new Exception("Don't find user!!"));
        return newUser;
    }

    @NotNull
    @Override
    public User findOneById(@NotNull final String userId) throws Exception {
        final Optional<User> optionalUser = userRepository.findById(userId);
        @NotNull final User newUser = optionalUser.orElseThrow(() -> new Exception("Don't find user!!"));
        return newUser;
    }

    @Override
    @Transactional
    public void remove(@NotNull final User user) {
        userRepository.delete(user);
    }

    @NotNull
    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    @Transactional
    public void removeAll() {
        userRepository.deleteAll();
    }

    @Nullable
    @Override
    public User findOneByLogin(@NotNull final String login) {
        return userRepository.findUserByLogin(login);
    }

    @Nullable
    @Override
    public User findOneByLoginPass(@NotNull final String login, @NotNull final String pass) {
        return userRepository.findUserByLoginAndPasswordHash(login, pass);
    }

    @NotNull
    private Domain loadBinaryDomain() throws IOException, ClassNotFoundException {
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("domain.out"));
        @NotNull final Object object = objectInputStream.readObject();
        @NotNull final Domain domain = (Domain) object;
        objectInputStream.close();
        return domain;
    }

    @NotNull
    private Domain loadXMLDomain() throws IOException {
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        @NotNull final String readContent = new String(Files.readAllBytes(Paths.get("domain.xml")));
        return xmlMapper.readValue(readContent, Domain.class);
    }

    private Domain loadJSONDomain() throws IOException {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String readContent = new String(Files.readAllBytes(Paths.get("domain.json")));
        return objectMapper.readValue(readContent, Domain.class);
    }

    private Domain loadXMLJaxDomain() throws JAXBException {
        @NotNull final File file = new File("domainB.xml");
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        return (Domain) unmarshaller.unmarshal(file);
    }

    private Domain loadJSONJaxDomain() throws JAXBException {
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final File file = new File("domainB.json");
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        unmarshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        unmarshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        return (Domain) unmarshaller.unmarshal(file);
    }


    private void saveBinaryDomain(@NotNull final Domain domain) throws IOException {
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(
                new FileOutputStream("domain.out"));
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
    }

    private void saveXMLDomain(@NotNull final Domain domain) throws IOException {
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.writerWithDefaultPrettyPrinter().writeValue(new File("domain.xml"), domain);
    }

    private void saveJSONDomain(@NotNull final Domain domain) throws IOException {
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        mapper.writerWithDefaultPrettyPrinter().writeValue(new File("domain.json"), domain);
    }

    private void saveXMLJaxDomain(@NotNull final Domain domain) throws JAXBException {
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(domain, new File("domainB.xml"));
    }

    private void saveJSONJaxDomain(@NotNull final Domain domain) throws JAXBException {
        System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(Domain.class);
        @NotNull final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        marshaller.marshal(domain, new File("domainB.json"));
    }

}
