package ru.kuchukov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kuchukov.tm.api.service.IPurposeService;
import ru.kuchukov.tm.entity.PurposeEntity;
import ru.kuchukov.tm.entity.User;
import ru.kuchukov.tm.enumeration.SortField;

import java.util.List;

@NoArgsConstructor
public abstract class PurposeService<T extends PurposeEntity> extends AbstractService<T> implements IPurposeService<T> {

    @Override
    public abstract T save(@NotNull T t);

    @NotNull
    @Override
    public abstract T findOneByEntity(@NotNull T t) throws Exception;

    @Nullable
    @Override
    public abstract T findOneByUser(@NotNull final User user, @NotNull final T t) throws Exception;

    @NotNull
    @Override
    public abstract T findOneById(@NotNull String id) throws Exception;

    @Override
    public abstract void remove(@NotNull T t);

    @Override
    public abstract void removeByUser(@NotNull final User user, @NotNull final T t);

    @Override
    public abstract @NotNull List<T> findAll();

    @Override
    public abstract @NotNull List<T> findAllByUser(@NotNull final User user);

    @NotNull
    @Override
    public abstract List<T> findAllByUserSort(@NotNull final User user, @NotNull final SortField sortField);

    @NotNull
    @Override
    public abstract List<T> findAllByUserAndLine(@NotNull final User user, @NotNull final String line);

    @Override
    public abstract void removeAll();

    @Override
    public abstract void removeAllByUser(@NotNull final User user);
}
