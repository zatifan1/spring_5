package ru.kuchukov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kuchukov.tm.api.service.ISessionService;
import ru.kuchukov.tm.entity.Session;
import ru.kuchukov.tm.entity.User;
import ru.kuchukov.tm.repository.ISessionRepository;
import ru.kuchukov.tm.repository.IUserRepository;
import ru.kuchukov.tm.util.SaltUtil;
import ru.kuchukov.tm.util.SignatureUtil;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
public class SessionService extends AbstractService<Session> implements ISessionService<Session> {

    @Autowired
    private ISessionRepository sessionRepository;
    @Autowired
    private IUserRepository userRepository;

    @Override
    @Transactional
    public Session save(@NotNull final Session session) {
        return sessionRepository.save(session);
    }

    @NotNull
    @Override
    public Session findOneByEntity(@NotNull final Session session) throws Exception {
        @NotNull final Optional<Session> optionalSession = sessionRepository.findById(session.getId());
        return optionalSession.orElseThrow(() -> new Exception("Don't find session!"));
    }

    @NotNull
    @Override
    public Session findOneById(@NotNull String id) throws Exception {
        @NotNull final Optional<Session> optionalSession = sessionRepository.findById(id);
        return optionalSession.orElseThrow(() -> new Exception("Don't find session!"));
    }

    @Override
    @Transactional
    public void remove(@NotNull final Session session) {
        sessionRepository.delete(session);
    }

    @Override
    @NotNull
    public List<Session> findAll() {
        return sessionRepository.findAll();
    }

    @Override
    @Transactional
    public void removeAll() {
        sessionRepository.deleteAll();
    }

    @Override
    @Nullable
    @Transactional
    public Session getNewSession(@NotNull final User user) throws Exception {
        @NotNull final Optional<User> optionalUser = userRepository.findById(user.getId());
        @NotNull final User newUser = optionalUser.orElseThrow(() -> new Exception("Don't find user!"));
        @NotNull final Session session = new Session();
        session.setDate(new Date());
        session.setUser(newUser);
        @Nullable final String sign = SignatureUtil.sign(session, SaltUtil.SALT, SaltUtil.CYCLE);
        if (sign == null) throw new Exception("Sign is Null!");
        session.setSignature(sign);
        return sessionRepository.save(session);
    }

    @Override
    public void valid(@NotNull final Session session) throws Exception {
        @NotNull final Session newSession = new Session();
        newSession.setId(session.getId());
        newSession.setDate(session.getDate());
        newSession.setUser(session.getUser());
        @Nullable final String newSign = SignatureUtil.sign(newSession, SaltUtil.SALT, SaltUtil.CYCLE);
        if (newSign == null) throw new Exception("New Signature is null!");
        if (sessionRepository.existsSessionByIdAndSignature(newSession.getId(), newSign))
            throw new Exception("Not Valid Session!");
    }

    @Override
    @Nullable
    public Session getSession(@NotNull final Session session, @NotNull final User user) throws Exception {
        @NotNull final Optional<Session> optionalSession = sessionRepository.findByIdAndUser(session.getId(), user);
        @NotNull final Session newSession = optionalSession.orElseThrow(() -> new Exception("Don't find session!"));
        return newSession;
    }
}
