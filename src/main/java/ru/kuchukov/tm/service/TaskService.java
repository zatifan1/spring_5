package ru.kuchukov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kuchukov.tm.api.service.ITaskService;
import ru.kuchukov.tm.entity.Project;
import ru.kuchukov.tm.entity.Task;
import ru.kuchukov.tm.entity.User;
import ru.kuchukov.tm.enumeration.SortField;
import ru.kuchukov.tm.repository.ITaskRepository;

import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
public class TaskService extends PurposeService<Task> implements ITaskService<Task> {

    @Autowired
    private ITaskRepository taskRepository;

    @Override
    @Transactional
    public Task save(@NotNull final Task task) {
        return taskRepository.save(task);
    }

    @NotNull
    @Override
    public Task findOneByEntity(@NotNull final Task task) throws Exception {
        @NotNull final Optional<Task> optionalTask = taskRepository.findById(task.getId());
        @NotNull final Task newTask = optionalTask.orElseThrow(() -> new Exception("Don't find task!"));
        return newTask;
    }

    @NotNull
    @Override
    public Task findOneById(@NotNull final String id) throws Exception {
        @NotNull final Optional<Task> optionalTask = taskRepository.findById(id);
        @NotNull final Task newTask = optionalTask.orElseThrow(() -> new Exception("Don't find task!"));
        return newTask;
    }

    @Nullable
    @Override
    public Task findOneByUser(@NotNull final User user, @NotNull final Task task) throws Exception {
        @NotNull final Optional<Task> optionalTask = taskRepository.findByIdAndUser(task.getId(), user);
        @NotNull final Task newTask = optionalTask.orElseThrow(() -> new Exception("Don't find task!"));
        return newTask;
    }

    @Override
    @Transactional
    public void remove(@NotNull final Task task) {
        taskRepository.delete(task);
    }

    @Override
    @Transactional
    public void removeByUser(@NotNull final User user, @NotNull final Task task) {
        taskRepository.deleteByIdAndUser(task.getId(), user);
    }

    @NotNull
    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @NotNull
    @Override
    public List<Task> findAllByUser(@NotNull final User user) {
        return taskRepository.findAllByUser(user);
    }

    @NotNull
    @Override
    public List<Task> findAllByUserSort(@NotNull final User user, @NotNull final SortField sortField) {
        @NotNull final List<Task> tasks;
        switch (sortField) {
            case STATUS:
                tasks = taskRepository.findAllByUserOrderByBeginDate(user);
                break;
            case END_DATE:
                tasks = taskRepository.findAllByUserOrderByEndDate(user);
                break;
            case BEGIN_DATE:
                tasks = taskRepository.findAllByUserOrderByStatus(user);
                break;
            default:
                tasks = taskRepository.findAllByUser(user);
                break;
        }
        return tasks;
    }

    @NotNull
    @Override
    public List<Task> findAllByUserAndLine(@NotNull final User user, @NotNull final String line) {
        @NotNull final List<Task> tasks = taskRepository.findAllByUserAndNameContains(user, line);
        return tasks;
    }

    @NotNull
    @Override
    public List<Task> findAllByUserAndProject(@NotNull final User user, @Nullable final Project project) {
        @NotNull final List<Task> tasks = taskRepository.findAllByUserAndProject(user, project);
        return tasks;
    }

    @Override
    @Transactional
    public void removeAll() {
        taskRepository.deleteAll();
    }

    @Override
    @Transactional
    public void removeAllByUser(@NotNull final User user) {
        taskRepository.deleteAllByUser(user);
    }

    @Override
    @Transactional
    public void removeAllByUserAndProject(@NotNull final User user, @Nullable final Project project) {
        taskRepository.deleteAllByUserAndProject(user, project);
    }

}
