package ru.kuchukov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.kuchukov.tm.api.service.IService;
import ru.kuchukov.tm.entity.AbstractEntity;

import java.util.List;

@NoArgsConstructor
public abstract class AbstractService<T extends AbstractEntity> implements IService<T> {

    @Override
    public abstract T save(@NotNull final T t);

    @NotNull
    @Override
    public abstract T findOneByEntity(@NotNull final T t) throws Exception;

    @NotNull
    @Override
    public T findOneById(@NotNull final String id) throws Exception {
        return null;
    }

    @Override
    public abstract void remove(@NotNull final T t);

    @NotNull
    @Override
    public abstract List<T> findAll();

    @Override
    public abstract void removeAll();

}
