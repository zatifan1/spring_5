package ru.kuchukov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kuchukov.tm.api.service.IRoleEntityService;
import ru.kuchukov.tm.entity.RoleEntity;
import ru.kuchukov.tm.entity.User;
import ru.kuchukov.tm.repository.IRoleEntityRepository;

import java.util.List;
import java.util.Optional;

@Service
@NoArgsConstructor
public class RoleEntityService extends AbstractService<RoleEntity> implements IRoleEntityService<RoleEntity> {

    @Autowired
    private IRoleEntityRepository roleEntityRepository;

    @Override
    public RoleEntity save(@NotNull final RoleEntity roleEntity) {
        return roleEntityRepository.save(roleEntity);
    }

    @NotNull
    @Override
    public RoleEntity findOneByEntity(@NotNull final RoleEntity roleEntity) throws Exception {
        final Optional<RoleEntity> optionalRole = roleEntityRepository.findById(roleEntity.getId());
        @NotNull final RoleEntity role = optionalRole.orElseThrow(() -> new Exception("Don't find RoleEntity!!"));
        return role;
    }

    @NotNull
    @Override
    public RoleEntity findOneById(@NotNull final String id) throws Exception {
        final Optional<RoleEntity> optionalRole = roleEntityRepository.findById(id);
        @NotNull final RoleEntity role = optionalRole.orElseThrow(() -> new Exception("Don't find RoleEntity!!"));
        return role;
    }

    @Override
    public void remove(@NotNull final RoleEntity roleEntity) {
        roleEntityRepository.delete(roleEntity);
    }

    @Override
    public @NotNull List<RoleEntity> findAll() {
        return roleEntityRepository.findAll();
    }

    @Override
    public @Nullable List<RoleEntity> findAllByUser(@NotNull final User user) {
        return roleEntityRepository.findAllByUser(user);
    }

    @Override
    public void removeAll() {
        roleEntityRepository.deleteAll();
    }
}
