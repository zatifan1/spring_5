package ru.kuchukov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kuchukov.tm.api.service.IProjectCommentService;
import ru.kuchukov.tm.entity.Project;
import ru.kuchukov.tm.entity.ProjectComment;
import ru.kuchukov.tm.repository.IProjectCommentRepository;

import java.util.List;
import java.util.Optional;

@Service
public class ProjectCommentService extends AbstractService<ProjectComment> implements IProjectCommentService<ProjectComment> {

    @Autowired
    private IProjectCommentRepository projectCommentRepository;

    @Override
    public ProjectComment save(@NotNull final ProjectComment projectComment) {
        return projectCommentRepository.save(projectComment);
    }

    @NotNull
    @Override
    public ProjectComment findOneByEntity(@NotNull final ProjectComment projectComment) throws Exception {
        @NotNull final Optional<ProjectComment> commentOptional = projectCommentRepository.findById(projectComment.getId());
        @NotNull final ProjectComment comment = commentOptional.orElseThrow(() -> new Exception("Don't find Project comment!!"));
        return comment;
    }

    @NotNull
    @Override
    public List<ProjectComment> findAllByProject(@NotNull final Project project) {
        return projectCommentRepository.findAllByProject(project);
    }

    @Override
    public void remove(@NotNull final ProjectComment projectComment) {
        projectCommentRepository.delete(projectComment);
    }

    @Override
    public @NotNull List<ProjectComment> findAll() {
        return projectCommentRepository.findAll();
    }

    @Override
    public void removeAll() {
        projectCommentRepository.deleteAll();
    }
}
