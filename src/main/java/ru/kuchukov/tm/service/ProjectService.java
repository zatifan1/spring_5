package ru.kuchukov.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kuchukov.tm.api.service.IProjectService;
import ru.kuchukov.tm.entity.Project;
import ru.kuchukov.tm.entity.User;
import ru.kuchukov.tm.enumeration.SortField;
import ru.kuchukov.tm.repository.IProjectRepository;

import java.util.List;
import java.util.Optional;


@Service
@NoArgsConstructor
public class ProjectService extends PurposeService<Project> implements IProjectService<Project> {

    @Autowired
    private IProjectRepository projectRepository;

    @Override
    @Transactional
    public Project save(@NotNull final Project project) {
        return projectRepository.save(project);
    }

    @NotNull
    @Override
    public Project findOneByEntity(@NotNull final Project project) throws Exception {
        Optional<Project> optionalProject = projectRepository.findById(project.getId());
        @NotNull final Project newProject = optionalProject.orElseThrow(() -> new Exception("Don't find project!!"));
        return newProject;
    }

    @NotNull
    @Override
    public Project findOneById(@NotNull final String id) throws Exception {
        Optional<Project> optionalProject = projectRepository.findById(id);
        @NotNull final Project newProject = optionalProject.orElseThrow(() -> new Exception("Don't find project!!"));
        return newProject;
    }

    @Nullable
    @Override
    public Project findOneByUser(@NotNull final User user, @NotNull final Project project) throws Exception {
        Optional<Project> optionalProject = projectRepository.findByIdAndUser(project.getId(), user);
        @Nullable final Project newProject = optionalProject.orElseThrow(() -> new Exception("Don't find project!!"));
        return newProject;
    }

    @Override
    @Transactional
    public void remove(@NotNull final Project project) {
        projectRepository.delete(project);
    }

    @Override
    public void removeByUser(@NotNull final User user, @NotNull final Project project) {
        projectRepository.deleteByIdAndUser(project.getId(), user);
    }

    @NotNull
    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public @NotNull List<Project> findAllByUser(@NotNull final User user) {
        return projectRepository.findAllByUser(user);
    }

    @NotNull
    @Override
    public List<Project> findAllByUserAndLine(@NotNull final User user, @NotNull final String line) {
        return projectRepository.findAllByUserAndNameContains(user, line);
    }

    @NotNull
    @Override
    public List<Project> findAllByUserSort(@NotNull final User user, @NotNull final SortField sortField) {
        @NotNull final List<Project> projects;
        switch (sortField) {
            case BEGIN_DATE:
                projects = projectRepository.findAllByUserOrderByBeginDate(user);
                break;
            case END_DATE:
                projects = projectRepository.findAllByUserOrderByEndDate(user);
                break;
            case STATUS:
                projects = projectRepository.findAllByUserOrderByStatus(user);
                break;
            default:
                projects = projectRepository.findAllByUser(user);
                break;
        }
        return projects;
    }

    @Override
    public void removeAll() {
        projectRepository.deleteAll();
    }

    @Override
    public void removeAllByUser(@NotNull final User user) {
        projectRepository.deleteAllByUser(user);
    }

}
