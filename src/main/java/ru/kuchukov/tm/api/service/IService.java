package ru.kuchukov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.kuchukov.tm.entity.AbstractEntity;

import java.util.List;

@Component
public interface IService<T extends AbstractEntity> {

    T save(@NotNull final T t);

    @NotNull
    T findOneByEntity(@NotNull final T t) throws Exception;

    @NotNull
    T findOneById(@NotNull final String id) throws Exception;

    void remove(@NotNull final T t);

    @NotNull
    List<T> findAll();

    void removeAll();

}
