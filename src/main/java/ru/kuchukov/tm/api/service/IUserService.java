package ru.kuchukov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.kuchukov.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@Component
@WebService
public interface IUserService<T extends User> extends IService<T> {

    @Override
    @WebMethod
    T save(@NotNull final User user);

    @NotNull
    @Override
    @WebMethod
    T findOneByEntity(@NotNull final User user) throws Exception;

    @NotNull
    @Override
    @WebMethod
    T findOneById(@NotNull final String id) throws Exception;

    @Override
    @WebMethod
    @NotNull
    List<T> findAll();

    @Override
    @WebMethod
    void remove(@NotNull final User user);

    @Override
    @WebMethod
    void removeAll();

    @Nullable
    @WebMethod
    User findOneByLogin(@NotNull final String login);

    @Nullable
    @WebMethod
    User findOneByLoginPass(@NotNull final String login, @NotNull final String passHash);

}
