package ru.kuchukov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.kuchukov.tm.entity.Session;
import ru.kuchukov.tm.entity.User;

import java.util.List;

@Component
public interface ISessionService<T extends Session> extends IService<T> {

    @Override
    T save(@NotNull final Session session);

    @NotNull
    @Override
    T findOneByEntity(@NotNull final Session session) throws Exception;

    @NotNull
    @Override
    T findOneById(final @NotNull String id) throws Exception;

    @Override
    @NotNull List<T> findAll();

    @Override
    void remove(@NotNull final Session session);

    @Override
    void removeAll();

    @Nullable
    Session getNewSession(@NotNull final User user) throws Exception;

    @Nullable
    void valid(@NotNull final Session session) throws Exception;

    @Nullable
    Session getSession(@NotNull final Session session, @NotNull final User user) throws Exception;

}
