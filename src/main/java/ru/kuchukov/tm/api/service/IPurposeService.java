package ru.kuchukov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.kuchukov.tm.entity.PurposeEntity;
import ru.kuchukov.tm.entity.User;
import ru.kuchukov.tm.enumeration.SortField;

import java.util.List;

@Component
public interface IPurposeService<T extends PurposeEntity> extends IService<T> {

    @Override
    T save(@NotNull final T t);

    @NotNull
    @Override
    T findOneByEntity(@NotNull final T t) throws Exception;

    @NotNull
    @Override
    T findOneById(final @NotNull String id) throws Exception;

    @Nullable
    T findOneByUser(@NotNull final User user, @NotNull final T t) throws Exception;

    @Override
    void remove(@NotNull final T t);

    void removeByUser(@NotNull final User user, @NotNull final T t);

    void removeAllByUser(@NotNull final User user);

    @NotNull
    @Override
    List<T> findAll();

    @NotNull
    List<T> findAllByUser(@NotNull final User user);

    @NotNull
    List<T> findAllByUserAndLine(@NotNull final User user, @NotNull final String line);

    @NotNull
    List<T> findAllByUserSort(@NotNull final User user, @NotNull final SortField sortField);

    @Override
    void removeAll();

}
