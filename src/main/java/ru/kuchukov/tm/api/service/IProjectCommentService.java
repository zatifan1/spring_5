package ru.kuchukov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.kuchukov.tm.entity.Project;
import ru.kuchukov.tm.entity.ProjectComment;

import java.util.List;

public interface IProjectCommentService<T extends ProjectComment> extends IService<T> {
    @Override
    T save(@NotNull final ProjectComment projectComment);

    @NotNull
    @Override
    T findOneByEntity(@NotNull final ProjectComment projectComment) throws Exception;

    @NotNull
    @Override
    T findOneById(final @NotNull String id) throws Exception;

    @NotNull
    List<T> findAllByProject(final @NotNull Project project) throws Exception;

    @Override
    void remove(@NotNull final ProjectComment projectComment);

    @Override
    @NotNull List<T> findAll();

    @Override
    void removeAll();
}
