package ru.kuchukov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.kuchukov.tm.entity.Project;
import ru.kuchukov.tm.entity.User;
import ru.kuchukov.tm.enumeration.SortField;

import java.util.List;

@Component
public interface IProjectService<T extends Project> extends IPurposeService<T> {

    @Override
    T save(@NotNull final Project project);

    @NotNull
    @Override
    T findOneByEntity(@NotNull final Project project) throws Exception;

    @NotNull
    @Override
    T findOneById(final @NotNull String id) throws Exception;

    @Nullable
    @Override
    T findOneByUser(@NotNull final User user, @NotNull final Project project) throws Exception;

    @Override
    void remove(@NotNull final Project project);

    @Override
    void removeByUser(@NotNull final User user, @NotNull final Project project);

    @NotNull
    @Override
    List<T> findAll();

    @NotNull
    @Override
    List<T> findAllByUser(@NotNull final User user);

    @NotNull
    @Override
    List<T> findAllByUserAndLine(@NotNull final User user, @NotNull final String line);

    @NotNull
    @Override
    List<T> findAllByUserSort(@NotNull final User user, @NotNull final SortField sortField);

    @Override
    void removeAll();

    @Override
    void removeAllByUser(@NotNull final User user);

}
