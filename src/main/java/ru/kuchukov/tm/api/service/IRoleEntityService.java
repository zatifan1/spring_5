package ru.kuchukov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.kuchukov.tm.entity.RoleEntity;
import ru.kuchukov.tm.entity.User;

import java.util.List;

@Component
public interface IRoleEntityService<T extends RoleEntity> extends IService<T> {

    @Override
    T save(@NotNull final RoleEntity roleEntity);

    @NotNull
    @Override
    T findOneByEntity(@NotNull final RoleEntity roleEntity) throws Exception;

    @NotNull
    @Override
    T findOneById(final @NotNull String id) throws Exception;

    @Override
    void remove(@NotNull final RoleEntity roleEntity);

    @Override
    @NotNull List<T> findAll();

    @Override
    void removeAll();

    @Nullable List<RoleEntity> findAllByUser(@NotNull final User user);

}
