package ru.kuchukov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.kuchukov.tm.entity.Project;
import ru.kuchukov.tm.entity.Task;
import ru.kuchukov.tm.entity.User;
import ru.kuchukov.tm.enumeration.SortField;

import java.util.List;

@Component
public interface ITaskService<T extends Task> extends IPurposeService<T> {

    @Override
    T save(@NotNull final Task task);

    @NotNull
    @Override
    T findOneByEntity(@NotNull final Task task) throws Exception;

    @NotNull
    @Override
    T findOneById(final @NotNull String id) throws Exception;

    @Nullable
    @Override
    T findOneByUser(@NotNull final User user, @NotNull final Task task) throws Exception;

    @Override
    void remove(@NotNull final Task task);

    @Override
    void removeByUser(@NotNull final User user, @NotNull final Task task);

    @Override
    @NotNull
    List<T> findAll();

    @Override
    @NotNull
    List<T> findAllByUser(@NotNull final User user);

    @Override
    @NotNull
    List<T> findAllByUserSort(@NotNull final User user, @NotNull final SortField sortField);

    @NotNull
    @Override
    List<T> findAllByUserAndLine(@NotNull final User user, @NotNull final String line);

    @NotNull
    List<T> findAllByUserAndProject(@NotNull final User user, @Nullable final Project project);

    @Override
    void removeAll();

    @Override
    void removeAllByUser(@NotNull final User user);

    void removeAllByUserAndProject(@NotNull final User user, @Nullable final Project project);

}
