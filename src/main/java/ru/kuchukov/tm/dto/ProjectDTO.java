package ru.kuchukov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.kuchukov.tm.enumeration.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class ProjectDTO extends AbstractDTO {

    @Nullable
    private String name;

    @Nullable
    private String description;

    @Nullable
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private Date beginDate;

    @Nullable
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private Date endDate;

    @Nullable
    private String userId;

    @NotNull
    private Status status = Status.PLANNED;

}
