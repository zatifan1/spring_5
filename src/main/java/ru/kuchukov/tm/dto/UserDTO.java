package ru.kuchukov.tm.dto;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.kuchukov.tm.entity.RoleEntity;
import ru.kuchukov.tm.enumeration.Role;

import javax.xml.bind.annotation.XmlElementWrapper;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDTO extends AbstractDTO {

    @Nullable
    private String login;

    @Nullable
    private String password;

    @Nullable
    private List<Role> roles = new ArrayList<>();

}
