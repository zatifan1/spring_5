package ru.kuchukov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class SessionDTO extends AbstractDTO {

    @Nullable
    private String userId;

    @Nullable
    private Date date;

}
