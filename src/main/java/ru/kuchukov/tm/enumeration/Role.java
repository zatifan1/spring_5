package ru.kuchukov.tm.enumeration;

import org.jetbrains.annotations.NotNull;

public enum Role {
    ADMIN(3, "admin"),
    MANAGER(2, "manager"),
    USER(1, "user");

    int lvl;
    @NotNull String role;

    Role(final int lvl, @NotNull final String role) {
        this.lvl = lvl;
        this.role = role;
    }

    public int getLvl() {
        return lvl;
    }

    @NotNull
    public String getRole() {
        return role;
    }

    public void setLvl(final int lvl) {
        this.lvl = lvl;
    }

    public void setRole(@NotNull final String role) {
        this.role = role;
    }
}
