package ru.kuchukov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.kuchukov.tm.entity.Project;
import ru.kuchukov.tm.entity.Task;
import ru.kuchukov.tm.entity.User;

import java.util.List;
import java.util.Optional;

@Repository
public interface ITaskRepository extends JpaRepository<Task, String> {

    Optional<Task> findByIdAndUser(@NotNull final String id, @NotNull final User user);

    List<Task> findAllByUser(@NotNull final User user);

    List<Task> findAllByUserOrderByBeginDate(@NotNull final User user);

    List<Task> findAllByUserOrderByEndDate(@NotNull final User user);

    List<Task> findAllByUserOrderByStatus(@NotNull final User user);

    List<Task> findAllByUserAndNameContains(@NotNull final User user, @NotNull final String line);

    List<Task> findAllByUserAndProject(@NotNull final User user, @Nullable final Project project);

    void deleteByIdAndUser(@NotNull final String id, @NotNull final User user);

    void deleteAllByUser(@NotNull final User user);

    void deleteAllByUserAndProject(@NotNull final User user, @Nullable final Project project);
}
