package ru.kuchukov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.kuchukov.tm.entity.User;

@Repository
public interface IUserRepository extends JpaRepository<User, String> {

    @Nullable User findUserByLogin(@NotNull final String login);

    @Nullable User findUserByLoginAndPasswordHash(@NotNull final String login, @NotNull final String passwordHash);

}
