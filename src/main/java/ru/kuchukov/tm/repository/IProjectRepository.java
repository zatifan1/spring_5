package ru.kuchukov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.kuchukov.tm.entity.Project;
import ru.kuchukov.tm.entity.User;

import java.util.List;
import java.util.Optional;

@Repository
public interface IProjectRepository extends JpaRepository<Project, String> {

    Optional<Project> findByIdAndUser(@NotNull final String id, @NotNull final User user);

    List<Project> findAllByUser(@NotNull final User user);

    List<Project> findAllByUserAndNameContains(@NotNull final User user, @NotNull final String line);

    List<Project> findAllByUserOrderByBeginDate(@NotNull final User user);

    List<Project> findAllByUserOrderByEndDate(@NotNull final User user);

    List<Project> findAllByUserOrderByStatus(@NotNull final User user);

    void deleteAllByUser(@NotNull final User user);

    void deleteByIdAndUser(@NotNull final String id, @Nullable final User user);

}
