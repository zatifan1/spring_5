package ru.kuchukov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.kuchukov.tm.entity.RoleEntity;
import ru.kuchukov.tm.entity.User;

import java.util.List;

@Repository
public interface IRoleEntityRepository extends JpaRepository<RoleEntity, String> {

    @NotNull List<RoleEntity> findAllByUser(User user);
}
