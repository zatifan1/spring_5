package ru.kuchukov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.kuchukov.tm.entity.Session;
import ru.kuchukov.tm.entity.User;

import java.util.Optional;

@Repository
public interface ISessionRepository extends JpaRepository<Session, String> {

    boolean existsSessionByIdAndSignature(@NotNull final String id, @NotNull final String signature);

    Optional<Session> findByIdAndUser(@NotNull final String id, @NotNull final User user);

}
