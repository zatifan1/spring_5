package ru.kuchukov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.kuchukov.tm.entity.Project;
import ru.kuchukov.tm.entity.ProjectComment;

import java.util.List;

@Repository
public interface IProjectCommentRepository extends JpaRepository<ProjectComment, String> {

    List<ProjectComment> findAllByProject(@NotNull final Project project);

}
