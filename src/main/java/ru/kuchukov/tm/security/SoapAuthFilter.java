package ru.kuchukov.tm.security;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import ru.kuchukov.tm.dto.UserDTO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.BufferedReader;
import java.io.IOException;


@NoArgsConstructor
public class SoapAuthFilter extends UsernamePasswordAuthenticationFilter {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        try {
            BufferedReader reader = request.getReader();
            JAXBContext jaxbContext = JAXBContext.newInstance(UserDTO.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            @Nullable final UserDTO userDTO = (UserDTO) unmarshaller.unmarshal(reader);
            System.out.println(reader);
            System.out.println("sadfasdasdsfdfaskdfaskjaasd-");
            UsernamePasswordAuthenticationToken token
                    = new UsernamePasswordAuthenticationToken("aa", "aa");
            setDetails(request, token);
            return this.getAuthenticationManager().authenticate(token);
        } catch (IOException | JAXBException e) {
            throw new InternalAuthenticationServiceException("error fuk");
        }
    }
}
