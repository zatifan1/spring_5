package ru.kuchukov.tm.security;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import ru.kuchukov.tm.api.service.IRoleEntityService;
import ru.kuchukov.tm.api.service.IUserService;
import ru.kuchukov.tm.entity.RoleEntity;
import ru.kuchukov.tm.entity.User;

import java.util.ArrayList;
import java.util.List;

public class UserDetailServiceImpl implements UserDetailsService {

    @Autowired
    private IUserService<User> userService;

    @Autowired
    private IRoleEntityService<RoleEntity> roleEntityService;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UsernameNotFoundException("User not found!");
        org.springframework.security.core.userdetails.User.UserBuilder userBuilder = null;
        userBuilder = org.springframework.security.core.userdetails.User.withUsername(login);
        userBuilder.password(user.getPasswordHash());
        @NotNull final List<RoleEntity> userRoles = roleEntityService.findAllByUser(user);
        @NotNull final List<String> roles = new ArrayList<>();
        for (@NotNull final RoleEntity role : userRoles) {
            roles.add(role.getRole().name());
        }
        userBuilder.roles(roles.toArray(new String[]{}));
        @NotNull final UserDetails userDetails = userBuilder.build();
        @Nullable org.springframework.security.core.userdetails.User result = null;
        result = (org.springframework.security.core.userdetails.User) userDetails;
        @Nullable final CustomUser customUser = new CustomUser(result);
        customUser.setUserId(user.getId());
        return customUser;
    }

    private User findByLogin(String login) {
        if (login == null || login.isEmpty()) return null;
        return userService.findOneByLogin(login);
    }

}
