package ru.kuchukov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public class PurposeEntity extends AbstractEntity implements Serializable {

    private static final long serialVersionUID = 4035276004503442617L;

    @Nullable
    private String name;

    @Nullable
    private String description;

    @Nullable
    @Column(name = "begin_date")
    private Date beginDate;

    @Nullable
    @Column(name = "end_date")
    private Date endDate;

    @Nullable
    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

}
