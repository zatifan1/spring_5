package ru.kuchukov.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kuchukov.tm.enumeration.Status;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_task")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Task extends PurposeEntity implements Serializable {

    private static final long serialVersionUID = 403527600450344261L;

    @Nullable
    @ManyToOne(fetch = FetchType.LAZY)
    private Project project;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private Status status = Status.PLANNED;

}
