package ru.kuchukov.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kuchukov.tm.enumeration.Role;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_role", uniqueConstraints = @UniqueConstraint(columnNames = {"Role","user_id"}))
@JsonIgnoreProperties(ignoreUnknown = true)
public class RoleEntity extends AbstractEntity implements Serializable {

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "Role", nullable = false)
    private Role role = Role.USER;

    @Nullable
    @JoinColumn(name = "user_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

}
