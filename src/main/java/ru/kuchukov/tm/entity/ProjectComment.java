package ru.kuchukov.tm.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_project_comment")
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProjectComment extends AbstractEntity {

    @Nullable
    private String comment;

    @Nullable
    @JoinColumn(name = "project_id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Project project;

}
