package ru.kuchukov.tm.endpointservice;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kuchukov.tm.api.service.IProjectService;
import ru.kuchukov.tm.dto.ProjectDTO;
import ru.kuchukov.tm.dto.UserDTO;
import ru.kuchukov.tm.entity.Project;
import ru.kuchukov.tm.entity.User;
import ru.kuchukov.tm.enumeration.SortField;
import ru.kuchukov.tm.util.DTOUtil;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@Component
@WebService
public class ProjectServiceEP {

    @Autowired
    private IProjectService projectService;

    @Autowired
    private DTOUtil dtoUtil;

    @WebMethod
    public @Nullable ProjectDTO save(@NotNull final ProjectDTO projectDTO) throws Exception {
        @Nullable final Project project = dtoUtil.getProjectFromProjectDTO(projectDTO);
        @Nullable final Project newProject = projectService.save(project);
        return dtoUtil.getProjectDTOFromProject(newProject);
    }

    @WebMethod
    public @Nullable ProjectDTO findOneByProject(@NotNull final ProjectDTO projectDTO) throws Exception {
        @Nullable final Project project = dtoUtil.getProjectFromProjectDTO(projectDTO);
        @Nullable final Project newProject = projectService.findOneByEntity(project);
        return dtoUtil.getProjectDTOFromProject(newProject);
    }

    @WebMethod
    public @Nullable ProjectDTO findOneById(@NotNull final String id) throws Exception {
        @NotNull final Project newProject = projectService.findOneById(id);
        return dtoUtil.getProjectDTOFromProject(newProject);
    }

    @WebMethod
    public @Nullable ProjectDTO findOneByUser(@NotNull final UserDTO userDTO, @NotNull final ProjectDTO projectDTO) throws Exception {
        @Nullable final User user = dtoUtil.getUserFromUserDTO(userDTO);
        @Nullable final Project project = dtoUtil.getProjectFromProjectDTO(projectDTO);
        @Nullable final Project newProject = projectService.findOneByUser(user, project);
        return dtoUtil.getProjectDTOFromProject(newProject);
    }

    @WebMethod
    public void remove(@NotNull final ProjectDTO projectDTO) throws Exception {
        @Nullable final Project project = dtoUtil.getProjectFromProjectDTO(projectDTO);
        projectService.remove(project);
    }

    @WebMethod
    public void removeByUser(@NotNull final UserDTO userDTO, @NotNull final ProjectDTO projectDTO) throws Exception {
        @Nullable final User user = dtoUtil.getUserFromUserDTO(userDTO);
        @Nullable final Project project = dtoUtil.getProjectFromProjectDTO(projectDTO);
        projectService.removeByUser(user, project);
    }

    @WebMethod
    public @NotNull List<ProjectDTO> findAll() {
        @NotNull final List<Project> projects = projectService.findAll();
        @NotNull final List<ProjectDTO> projectDTOS = new ArrayList<>();
        for (@NotNull final Project project : projects) {
            projectDTOS.add(dtoUtil.getProjectDTOFromProject(project));
        }
        return projectDTOS;
    }

    @WebMethod
    public @NotNull List<ProjectDTO> findAllByUser(@NotNull final UserDTO userDTO) throws Exception {
        @Nullable final User user = dtoUtil.getUserFromUserDTO(userDTO);
        @NotNull final List<Project> projects = projectService.findAllByUser(user);
        @NotNull final List<ProjectDTO> projectDTOS = new ArrayList<>();
        for (@NotNull final Project project : projects) {
            projectDTOS.add(dtoUtil.getProjectDTOFromProject(project));
        }
        return projectDTOS;
    }

    @WebMethod
    public @NotNull List<ProjectDTO> findAllByUserAndLine(@NotNull final UserDTO userDTO, @NotNull final String line) throws Exception {
        @Nullable final User user = dtoUtil.getUserFromUserDTO(userDTO);
        @NotNull final List<Project> projects = projectService.findAllByUserAndLine(user, line);
        @NotNull final List<ProjectDTO> projectDTOS = new ArrayList<>();
        for (@NotNull final Project project : projects) {
            projectDTOS.add(dtoUtil.getProjectDTOFromProject(project));
        }
        return projectDTOS;
    }

    @WebMethod
    public @NotNull List<ProjectDTO> findAllByUserSort(@NotNull final UserDTO userDTO, @NotNull final SortField sortField) throws Exception {
        @Nullable final User user = dtoUtil.getUserFromUserDTO(userDTO);
        @NotNull final List<Project> projects = projectService.findAllByUserSort(user, sortField);
        @NotNull final List<ProjectDTO> projectDTOS = new ArrayList<>();
        for (@NotNull final Project project : projects) {
            projectDTOS.add(dtoUtil.getProjectDTOFromProject(project));
        }
        return projectDTOS;
    }

    @WebMethod
    public void removeAll() {
        projectService.removeAll();
    }

    @WebMethod
    public void removeAllByUser(@NotNull UserDTO userDTO) throws Exception {
        @Nullable final User user = dtoUtil.getUserFromUserDTO(userDTO);
        projectService.removeAllByUser(user);
    }
}
