package ru.kuchukov.tm.endpointservice;

import org.jetbrains.annotations.NotNull;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import ru.kuchukov.tm.dto.UserDTO;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebService;

@Component
@WebService
public class AuthServiceEP {

    @Resource
    private AuthenticationManager authenticationManager;

    @WebMethod
    public void auth(@NotNull final UserDTO userDTO) {
        final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken(userDTO.getLogin(), userDTO.getPassword());
        final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }


}
