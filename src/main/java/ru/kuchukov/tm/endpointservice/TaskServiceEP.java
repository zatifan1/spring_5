package ru.kuchukov.tm.endpointservice;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kuchukov.tm.api.service.ITaskService;
import ru.kuchukov.tm.dto.ProjectDTO;
import ru.kuchukov.tm.dto.TaskDTO;
import ru.kuchukov.tm.dto.UserDTO;
import ru.kuchukov.tm.entity.*;
import ru.kuchukov.tm.enumeration.SortField;
import ru.kuchukov.tm.util.DTOUtil;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@Component
@WebService
public class TaskServiceEP {
    @Autowired
    private ITaskService<Task> taskService;

    @Autowired
    private DTOUtil dtoUtil;

    @WebMethod
    public @Nullable TaskDTO save(@NotNull final TaskDTO taskDTO) throws Exception {
        @Nullable final Task task = dtoUtil.getTaskFromTaskDTO(taskDTO);
        @Nullable final Task newTask = taskService.save(task);
        return dtoUtil.getTaskDTOFromTask(newTask);
    }

    @WebMethod
    public @Nullable TaskDTO findOneByEntity(@NotNull final TaskDTO taskDTO) throws Exception {
        @Nullable final Task task = dtoUtil.getTaskFromTaskDTO(taskDTO);
        @Nullable final Task newTask = taskService.findOneByEntity(task);
        return dtoUtil.getTaskDTOFromTask(newTask);
    }

    @WebMethod
    public @Nullable TaskDTO findOneById(@NotNull final String id) throws Exception {
        @Nullable final Task newTask = taskService.findOneById(id);
        return dtoUtil.getTaskDTOFromTask(newTask);
    }

    @WebMethod
    public @Nullable TaskDTO findOneByUser(@NotNull final UserDTO userDTO, @NotNull final TaskDTO taskDTO) throws Exception {
        @Nullable final User user = dtoUtil.getUserFromUserDTO(userDTO);
        @Nullable final Task task = dtoUtil.getTaskFromTaskDTO(taskDTO);
        @Nullable final Task newTask = taskService.findOneByUser(user, task);
        return dtoUtil.getTaskDTOFromTask(newTask);
    }

    @WebMethod
    public void remove(@NotNull final TaskDTO taskDTO) throws Exception {
        @Nullable final Task task = dtoUtil.getTaskFromTaskDTO(taskDTO);
        taskService.remove(task);
    }

    @WebMethod
    public void removeByUser(@NotNull final UserDTO userDTO, @NotNull final TaskDTO taskDTO) throws Exception {
        @Nullable final User user = dtoUtil.getUserFromUserDTO(userDTO);
        @Nullable final Task task = dtoUtil.getTaskFromTaskDTO(taskDTO);
        taskService.removeByUser(user, task);
    }

    @WebMethod
    public @NotNull List<TaskDTO> findAll() {
        @NotNull final List<Task> tasks = taskService.findAll();
        @NotNull final List<TaskDTO> taskDTOS = new ArrayList<>();
        for (@NotNull final Task task : tasks) {
            taskDTOS.add(dtoUtil.getTaskDTOFromTask(task));
        }
        return taskDTOS;
    }

    @WebMethod
    public @NotNull List<TaskDTO> findAllByUser(@NotNull final UserDTO userDTO) throws Exception {
        @Nullable final User user = dtoUtil.getUserFromUserDTO(userDTO);
        @NotNull final List<Task> tasks = taskService.findAllByUser(user);
        @NotNull final List<TaskDTO> taskDTOS = new ArrayList<>();
        for (@NotNull final Task task : tasks) {
            taskDTOS.add(dtoUtil.getTaskDTOFromTask(task));
        }
        return taskDTOS;
    }

    @WebMethod
    public @NotNull List<TaskDTO> findAllByUserSort(@NotNull final UserDTO userDTO, @NotNull final SortField sortField) throws Exception {
        @Nullable final User user = dtoUtil.getUserFromUserDTO(userDTO);
        @NotNull final List<Task> tasks = taskService.findAllByUserSort(user, sortField);
        @NotNull final List<TaskDTO> taskDTOS = new ArrayList<>();
        for (@NotNull final Task task : tasks) {
            taskDTOS.add(dtoUtil.getTaskDTOFromTask(task));
        }
        return taskDTOS;

    }

    @WebMethod
    public @NotNull List<TaskDTO> findAllByUserAndLine(@NotNull final UserDTO userDTO, @NotNull final String line) throws Exception {
        @Nullable final User user = dtoUtil.getUserFromUserDTO(userDTO);
        @NotNull final List<Task> tasks = taskService.findAllByUserAndLine(user, line);
        @NotNull final List<TaskDTO> taskDTOS = new ArrayList<>();
        for (@NotNull final Task task : tasks) {
            taskDTOS.add(dtoUtil.getTaskDTOFromTask(task));
        }
        return taskDTOS;
    }

    @WebMethod
    public @NotNull List<TaskDTO> findAllByUserAndProject(@NotNull final UserDTO userDTO, @Nullable final ProjectDTO projectDTO) throws Exception {
        @Nullable final User user = dtoUtil.getUserFromUserDTO(userDTO);
        @Nullable final Project project = dtoUtil.getProjectFromProjectDTO(projectDTO);
        @NotNull final List<Task> tasks = taskService.findAllByUserAndProject(user, project);
        @NotNull final List<TaskDTO> taskDTOS = new ArrayList<>();
        for (@NotNull final Task task : tasks) {
            taskDTOS.add(dtoUtil.getTaskDTOFromTask(task));
        }
        return taskDTOS;
    }

    @WebMethod
    public void removeAll() {
        taskService.removeAll();
    }

    @WebMethod
    public void removeAllByUser(@NotNull final UserDTO userDTO) throws Exception {
        @Nullable final User user = dtoUtil.getUserFromUserDTO(userDTO);
        taskService.removeAllByUser(user);
    }

    @WebMethod
    public void removeAllByUserAndProject(@NotNull final UserDTO userDTO, @Nullable final ProjectDTO projectDTO) throws Exception {
        @Nullable final User user = dtoUtil.getUserFromUserDTO(userDTO);
        @Nullable final Project project = dtoUtil.getProjectFromProjectDTO(projectDTO);
        taskService.removeAllByUserAndProject(user, project);
    }

}
