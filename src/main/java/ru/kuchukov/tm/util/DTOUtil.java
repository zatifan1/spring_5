package ru.kuchukov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ru.kuchukov.tm.dto.ProjectDTO;
import ru.kuchukov.tm.dto.SessionDTO;
import ru.kuchukov.tm.dto.TaskDTO;
import ru.kuchukov.tm.dto.UserDTO;
import ru.kuchukov.tm.entity.Project;
import ru.kuchukov.tm.entity.Session;
import ru.kuchukov.tm.entity.Task;
import ru.kuchukov.tm.entity.User;
import ru.kuchukov.tm.repository.IProjectRepository;
import ru.kuchukov.tm.repository.IUserRepository;

import java.util.Optional;

@Component
public class DTOUtil {
    @Autowired
    private IUserRepository userRepository;

    @Autowired
    private IProjectRepository projectRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;
    @Nullable
    public User getUserFromUserDTO(@Nullable final UserDTO userDTO) throws Exception {
        if (userDTO == null) return null;
        @NotNull final User user = new User();
        if (userDTO.getId() != null) user.setId(userDTO.getId());
        if (userDTO.getPassword() != null)
            user.setPasswordHash(passwordEncoder.encode(userDTO.getPassword()));
        else {
            @NotNull final Optional<User> optionalUser = userRepository.findById(user.getId());
            user.setPasswordHash(optionalUser.orElseThrow(() -> new Exception("Don't find user!")).getPasswordHash());
        }
        user.setLogin(userDTO.getLogin());
//        user.setRoles(userDTO.getRoles());
        return user;
    }

    @Nullable
    public UserDTO getUserDTOFromUser(@Nullable final User user) {
        if (user == null) return null;
        @NotNull final UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setLogin(user.getLogin());
//        userDTO.setRoles(user.getRoles());
        return userDTO;
    }

    @Nullable
    public Session getSessionFromSessionDTO(@Nullable final SessionDTO sessionDTO) throws Exception {
        if (sessionDTO == null) return null;
        @NotNull final Session session = new Session();
        if (sessionDTO.getId() != null) session.setId(sessionDTO.getId());
        if (sessionDTO.getUserId() == null) return null;

        @NotNull final Optional<User> optionalUser = userRepository.findById(sessionDTO.getUserId());
        @Nullable final User user = optionalUser.orElseThrow(() -> new Exception("Don't find user!"));
        session.setUser(user);

        session.setDate(sessionDTO.getDate());
        session.setSignature(SignatureUtil.sign(session, SaltUtil.SALT, SaltUtil.CYCLE));
        System.out.println(session.getSignature());
        return session;
    }

    @Nullable
    public SessionDTO getSessionDTOFromSession(@Nullable final Session session) {
        if (session == null) return null;
        @NotNull final SessionDTO sessionDTO = new SessionDTO();
        sessionDTO.setId(session.getId());
        if (session.getUser() == null) return null;
        sessionDTO.setUserId(session.getUser().getId());
        sessionDTO.setDate(session.getDate());
        return sessionDTO;
    }

    @Nullable
    public Project getProjectFromProjectDTO(@Nullable final ProjectDTO projectDTO) throws Exception {
        if (projectDTO == null) return null;
        @NotNull final Project project = new Project();
        if (projectDTO.getId() != null) project.setId(projectDTO.getId());
        project.setName(projectDTO.getName());
        project.setDescription(projectDTO.getDescription());
        project.setBeginDate(projectDTO.getBeginDate());
        project.setEndDate(projectDTO.getEndDate());

        @NotNull final Optional<User> optionalUser;
        if (projectDTO.getUserId() != null) {
            optionalUser = userRepository.findById(projectDTO.getUserId());
        } else throw new Exception("Project userId is Null!");
        @Nullable final User user = optionalUser.orElseThrow(() -> new Exception("Don't find user!"));

        project.setUser(user);
        project.setStatus(projectDTO.getStatus());
        return project;
    }

    @Nullable
    public ProjectDTO getProjectDTOFromProject(@Nullable final Project project) {
        if (project == null) return null;
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setId(project.getId());
        projectDTO.setName(project.getName());
        projectDTO.setDescription(project.getDescription());
        projectDTO.setBeginDate(project.getBeginDate());
        projectDTO.setEndDate(project.getEndDate());
        projectDTO.setUserId(project.getUser().getId());
        projectDTO.setStatus(project.getStatus());
        return projectDTO;
    }

    @Nullable
    public Task getTaskFromTaskDTO(@Nullable final TaskDTO taskDTO) throws Exception {
        if (taskDTO == null) return null;
        @NotNull final Task task = new Task();
        if (taskDTO.getId() != null) task.setId(taskDTO.getId());
        task.setName(taskDTO.getName());
        task.setDescription(taskDTO.getDescription());
        task.setBeginDate(taskDTO.getBeginDate());
        task.setEndDate(taskDTO.getEndDate());

        @NotNull final Optional<User> optionalUser;
        if (taskDTO.getUserId() != null) {
            optionalUser = userRepository.findById(taskDTO.getUserId());
        } else throw new Exception("Task userId is Null");
        @Nullable final User user = optionalUser.orElseThrow(() -> new Exception("Don't find user!"));
        task.setUser(user);
        if (taskDTO.getProjectId() != null) {
            Optional<Project> optionalProject = projectRepository.findById(taskDTO.getProjectId());
            task.setProject(optionalProject.orElseThrow(() -> new Exception("Don't find project")));
        } else task.setProject(null);
        task.setStatus(taskDTO.getStatus());
        return task;
    }

    @Nullable
    public TaskDTO getTaskDTOFromTask(@Nullable final Task task) {
        if (task == null) return null;
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskDTO.setId(task.getId());
        taskDTO.setName(task.getName());
        taskDTO.setDescription(task.getDescription());
        taskDTO.setBeginDate(task.getBeginDate());
        taskDTO.setEndDate(task.getEndDate());
        taskDTO.setUserId(task.getUser().getId());
        if (task.getProject() != null) taskDTO.setProjectId(task.getProject().getId());
        taskDTO.setStatus(task.getStatus());
        return taskDTO;
    }
}
