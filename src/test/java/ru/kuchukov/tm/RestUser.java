package ru.kuchukov.tm;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;
import ru.kuchukov.tm.conf.ApplicationConfiguration;
import ru.kuchukov.tm.dto.UserDTO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
public class RestUser {

    @Autowired
    private RestTemplate restTemplate;
    private String userUrl;
    private HttpHeaders headers;
    private ObjectMapper mapper;
    private UserDTO userDTO;

    @Before
    public void before()  throws JsonProcessingException {
        userUrl = "http://localhost:8080/rest/";
        headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        mapper = new ObjectMapper();
        userDTO = new UserDTO();

        userDTO.setLogin("login");
        userDTO.setPassword("login");
        HttpEntity<String> request = new HttpEntity<>(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(userDTO), headers);

        final ResponseEntity<UserDTO> userDTOResponseEntity = restTemplate.postForEntity(userUrl + "login", request, UserDTO.class);
        Assert.assertEquals(200, userDTOResponseEntity.getStatusCodeValue());
    }

    @Test(expected = HttpServerErrorException.InternalServerError.class)
    public void delete() throws JsonProcessingException {
        userUrl = "http://localhost:8080/rest/user/";
        HttpEntity<String> request = new HttpEntity<>(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(userDTO), headers);

        final ResponseEntity<UserDTO> userDTOResponseEntity = restTemplate.postForEntity(userUrl, request, UserDTO.class);
        @Nullable final UserDTO newUserDTO = userDTOResponseEntity.getBody();

        request = new HttpEntity<>(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(newUserDTO), headers);
        restTemplate.postForEntity(userUrl + "delete", request, UserDTO.class);

        final ResponseEntity<UserDTO> userDTOResponseEntity1 = restTemplate.getForEntity(userUrl + newUserDTO.getId(), UserDTO.class);

    }
}
